

function isRomanMinForm(roman)
	local rom = arab2rom(rom2arab(roman))
	if (#roman == #rom) then
		return true
	 end
end

romanArray = { 
	["M"] = 1000,
	["D"] = 500, 
	["C"] = 100, 
	["L"] = 50, 
	["X"] = 10, 
	["V"] = 5, 
	["I"] = 1,
}

function atChar(str, inxex)
	if (inxex == 0) then return nil end
	return string.char(str:byte(inxex))
end

function rom2arab(roman)
	local value = 0;
	local curChar;
	local prevChar;

	for i = 1, #roman do
		curChar = atChar(roman, i)		
		prevChar = atChar(roman, i - 1)

		if not(i == 1) and (romanArray[prevChar] < romanArray[curChar]) then
				value =  value - romanArray[prevChar] * 2			
		end
		value = value + romanArray[curChar]
	end
	return value;
end


--========================================================

romIndex = {1, 4, 5, 9, 10, 40, 50, 90,100, 400, 500, 900, 1000}

romanArray2 = {};
romanArray2[1000] = 'M'
romanArray2[900] = 'CM'
romanArray2[500] = 'D'
romanArray2[400] = 'CD'
romanArray2[100] = 'C'
romanArray2[90] = 'XC'
romanArray2[50] = 'L'
romanArray2[40] = 'XL'
romanArray2[10] = 'X'
romanArray2[9] = 'IX'
romanArray2[5] = 'V'
romanArray2[4] = 'IV'
romanArray2[1] = 'I'

function arab2rom(arab)
	local roman = "";
	 if not(romanArray2[arab] == nil) then
	 	return romanArray2[arab]	
	 else
	 	local valueTemp = 1;
	 	local index = #romIndex;
	 	while not (valueTemp == 0) do
		 	while index > 0 do
		 		valueTemp = (arab - romIndex[index]);
		 		if  valueTemp > 0 then
		 			arab = valueTemp;
		 			roman = roman .. romanArray2[romIndex[index]];
		 			break;
		 		elseif valueTemp == 0 then		 			
		 		  roman = roman..romanArray2[romIndex[index]]
		 		    break;
		 		end
		 		index = index -1
		 	end
		 end
	 end
	return roman;
end
