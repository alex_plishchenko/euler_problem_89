require "RomanNumerals"

fileName = "roman.txt"

file = io.open(fileName, "r")

local countRomanMinForm = 0;

if file == nil then
    print( "error open file:", fileName )
else
	 for line in io.lines(fileName) do
	 	if isRomanMinForm(line) then
	 		countRomanMinForm = countRomanMinForm + 1
	 	end
	end
	io.close(file)

	print( "countRomanMinForm:", countRomanMinForm )
end
